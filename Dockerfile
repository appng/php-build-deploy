FROM php:7.3.1

RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install -y ssh && \
    apt-get install -y git && \
    apt-get install -y unzip && \
    apt-get -y remove apt-utils && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*
